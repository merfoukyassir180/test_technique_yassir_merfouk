package com.ym.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Setter @Getter
public class CaseResponseDTO {
    private Long caseId;
    private LocalDate creationDate;
    private LocalDate lastUpdateDate;
    private String title;
    private String description;
}
