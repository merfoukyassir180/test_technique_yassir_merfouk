package com.ym.dto;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class CaseRequestDTO {

    private String title;
    private String description;
}
