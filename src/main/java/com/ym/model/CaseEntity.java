package com.ym.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor @AllArgsConstructor @Setter @Getter @Builder
public class CaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long caseId;
    private LocalDate creationDate;
    private LocalDate lastUpdateDate;
    private String title;
    private String description;

    public void copy(CaseEntity newCaseEntity){
        if(newCaseEntity.title != null) this.title = newCaseEntity.getTitle();
        if(newCaseEntity.description != null) this.description = newCaseEntity.getTitle();
    }

}
