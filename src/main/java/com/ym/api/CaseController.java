package com.ym.api;

import com.ym.dto.CaseRequestDTO;
import com.ym.dto.CaseResponseDTO;
import com.ym.service.ICaseService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cases")
@AllArgsConstructor
public class CaseController {

    private ICaseService caseService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CaseResponseDTO addCase(@RequestBody CaseRequestDTO caseRequestDTO){
        return caseService.addCase(caseRequestDTO);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CaseResponseDTO updateCase(
            @PathVariable("id") Long caseId,
            @RequestBody CaseRequestDTO caseRequestDTO
    ){
        return caseService.updateCase(caseId, caseRequestDTO);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CaseResponseDTO getCase(@PathVariable("id") Long caseId){
        return caseService.getCase(caseId);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCase(@PathVariable("id") Long caseId){
        caseService.deleteCase(caseId);
    }
}
