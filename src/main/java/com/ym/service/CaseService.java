package com.ym.service;

import com.ym.dto.CaseRequestDTO;
import com.ym.dto.CaseResponseDTO;
import com.ym.exception.CaseNotFoundException;
import com.ym.mapper.CaseMapper;
import com.ym.model.CaseEntity;
import com.ym.repository.CaseRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class CaseService implements ICaseService{

    private CaseRepository caseRepository;
    private CaseMapper caseMapper;

    @Override
    public CaseResponseDTO addCase(CaseRequestDTO caseRequestDTO){
        CaseEntity newCaseEntity = caseMapper.toCase(caseRequestDTO);
        newCaseEntity.setCreationDate(LocalDate.now());
        newCaseEntity.setLastUpdateDate(LocalDate.now());
        caseRepository.save(newCaseEntity);
        return caseMapper.toCaseResponseDTO(newCaseEntity);
    }

    @Override
    public CaseResponseDTO updateCase(Long caseId,CaseRequestDTO caseRequestDTO){
        CaseEntity findedCaseEntity = caseRepository.findById(caseId)
                .orElseThrow(() -> new CaseNotFoundException("Case " + caseId + " not found"));
        findedCaseEntity.copy(caseMapper.toCase(caseRequestDTO));
        findedCaseEntity.setLastUpdateDate(LocalDate.now());
        caseRepository.save(findedCaseEntity);
        return caseMapper.toCaseResponseDTO(findedCaseEntity);
    }

    @Override
    public CaseResponseDTO getCase(Long caseId) {
        CaseEntity findedCaseEntity = caseRepository.findById(caseId)
                .orElseThrow(() -> new CaseNotFoundException("Case " + caseId + " not found"));
        return caseMapper.toCaseResponseDTO(findedCaseEntity);
    }

    @Override
    public void deleteCase(Long caseId) {
        CaseEntity findedCaseEntity = caseRepository.findById(caseId)
                .orElseThrow(() -> new CaseNotFoundException("Case " + caseId + " not found"));
        caseRepository.delete(findedCaseEntity);
    }


}
