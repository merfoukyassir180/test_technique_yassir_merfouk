package com.ym.service;

import com.ym.dto.CaseRequestDTO;
import com.ym.dto.CaseResponseDTO;

public interface ICaseService {
    CaseResponseDTO addCase(CaseRequestDTO caseRequestDTO);
    CaseResponseDTO updateCase(Long caseId,CaseRequestDTO caseRequestDTO);
    CaseResponseDTO getCase(Long caseId);
    void deleteCase(Long caseId);

}
