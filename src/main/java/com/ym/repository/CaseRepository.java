package com.ym.repository;

import com.ym.model.CaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaseRepository extends JpaRepository<CaseEntity, Long> {

    boolean existsById(Long id);
}
