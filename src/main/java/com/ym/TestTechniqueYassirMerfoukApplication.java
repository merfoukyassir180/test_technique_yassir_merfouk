package com.ym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestTechniqueYassirMerfoukApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestTechniqueYassirMerfoukApplication.class, args);
	}

}
