package com.ym.exception;

public class CaseNotFoundException extends RuntimeException{
    public CaseNotFoundException(String msg){
        super(msg);
    }
}
