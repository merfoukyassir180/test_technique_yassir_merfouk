package com.ym.mapper;

import com.ym.dto.CaseRequestDTO;
import com.ym.dto.CaseResponseDTO;
import com.ym.model.CaseEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class CaseMapper {

    public CaseEntity toCase(CaseRequestDTO caseRequestDTO){
        CaseEntity newCaseEntity = new CaseEntity();
        BeanUtils.copyProperties(caseRequestDTO, newCaseEntity);
        return newCaseEntity;
    }

    public CaseResponseDTO toCaseResponseDTO(CaseEntity newCaseEntity){
        CaseResponseDTO caseResponseDTO = new CaseResponseDTO();
        BeanUtils.copyProperties(newCaseEntity, caseResponseDTO);
        return caseResponseDTO;
    }
}
